#! /usr/bin/env python3
# -*- coding:UTF-8 -*-

import pytest
from ..word import Word
from ... import util


#SET-UP
def dict1():
    return {"lemma":"eat","surface":"eater","pos":"noun"}

def dict2():
    return {"surface":"walks","lemma":"walk","pos":"verb","syn":"root:0"}

def dict3(): #incorrect one : "pos":int
    return {"surface":"@~&","lemma":"{/*","pos":123,"syn":"###:é&"}

def dict4():
    return{"surface":"Charles","lemma":"Charles","pos":"noun","syn":"nsubj:1"}

def dict5():
    return{"surface":"tests","lemma":"test","pos":"noun","syn":"obj:3;nsubj:12;az:7;fbkd:65"}

def dict6(): #incorrect one "syn":"obj:3;nsubj12" instead of "obj:3;nsubj:12"
    return{"surface":"tests","lemma":"test","pos":"noun","syn":"obj:3;nsubj12"}


"""
    Functions tested : __init__, get_prop, del_prop, set_prop, get_props, copy, lemma_or_surface, __eq__, __hash__, syn_encode, syn_iter
"""


#TESTS

#Word with an empty dictionary
def test_init_1():
    tmp_dct = dict()
    tmpWord=Word(tmp_dct)
    assert tmpWord._props == {}
    assert tmpWord.ctxinfo == None

#Word with a "normal" usage    
def test_init_2():
    tmpWord=Word(dict1(),ctxinfo="Try")
    assert tmpWord._props["lemma"] == "eat"
    assert tmpWord._props["surface"] == "eater"
    assert tmpWord._props["pos"] == "noun"
    assert tmpWord.ctxinfo == "Try"
    assert len(tmpWord._props) == 3

def test_get_prop1_1():
    #testing the lemma
    tmpWord=Word(dict1())
    assert tmpWord.get_prop("lemma") == "eat"

def test_get_prop1_2():
    tmpWord=Word(dict1())
    #10 isn't a key of dict 1
    assert tmpWord.get_prop("10","no key for 10") == "no key for 10" 
    assert tmpWord.get_prop("pos","no key for pos") != "no key for pos"

def test_get_prop1_3():
    tmpWord=Word(dict1())    
    #testing keyErrors
    with pytest.raises(KeyError):
        tmpWord.get_prop("Nope")
    with pytest.raises(KeyError):
        tmpWord.get_prop("syn")

def test_get_prop2_1():
    #testing with another data entry    
    tmpWord = Word(dict2())
    assert tmpWord.lemma==tmpWord.get_prop("lemma")
    assert tmpWord.get_prop("lemma")=="walk"
    assert tmpWord.lemma== "walk"

def test_get_prop2_2():
    tmpWord = Word(dict2())    
    #testing the surface
    assert tmpWord.surface=="walks"
    assert "walks"==tmpWord.get_prop("surface")

def test_get_prop2_3():
    tmpWord = Word(dict2())
    #testing the pos
    assert tmpWord.pos=="verb"
    assert tmpWord.get_prop("pos")=="verb"

    assert tmpWord.get_prop("syn")=="root:0"

def test_del_prop():
    #test if the lemma is properly deleted
    dic = dict2()
    w = Word(dic)
    w.del_prop("lemma")
    assert ("lemma" in w._props) == False
    assert ("lemma" in dic) == True
    


def test_set_prop_error1():
    #test TypeError because we put in the props key=int:value=str
    w = Word({})
    with pytest.raises(TypeError):
        w.set_prop(123, "lmao")

def test_set_prop_error2():
    #test TypeError because we put in the props key=str:value=int    
    w = Word({})
    with pytest.raises(TypeError):
        w.set_prop("surface", 123)

def test_set_prop_error3():
    #test TypeError because we put in the props key=str:value=boolean
    w = Word({})
    with pytest.raises(TypeError):
        w.set_prop("pos", True)

def test_set_prop_normal():
    #testing normal usage
    w = Word({})
    w.set_prop("lemma", "boy")
    assert ("lemma" in w._props) == True
    assert w._props["lemma"]=="boy"

def test_set_prop_overwrite():
    #testing the case where we overwrite a value (lemma is already defined)
    w = Word({})
    w.set_prop("lemma", "boy")
    w.set_prop("lemma", "grill")
    assert w._props["lemma"]!="boy"
    assert w._props["lemma"]=="grill"

def test_set_prop_delete():
    #testing the case where we delete a key + value
    w = Word({})
    w.set_prop("lemma", "grill")
    w.set_prop("lemma", None)
    assert ("lemma" in w._props) == False
    
def test_set_prop_check_dict():
    #test if the base dictionary doesn't change when we change a word prop 
    dic = dict4()
    w = Word(dic)
    w.set_prop("lemma","Lucina")
    assert w._props["lemma"] != dic["lemma"]
    assert dic["lemma"] == "Charles"

def test_get_props():
    #test if the key and value of the word are the correct
    w=Word(dict4())
    testProps = w.get_props()
    for key, value in dict4().items():
        assert (key in testProps) == True
        assert testProps[key] == value
    assert ("falconpunch" in testProps) == False
    #test the number of the keys are the same 
    assert len(dict4()) == len(testProps)

def test_copy1():
    w1= Word(dict4())
    w2 = w1.copy()
    
    #test if the word 2 is modified and not the word 1
    w2.set_prop("lemma","Diobrando")
    assert w2. _props["lemma"] != w1. _props["lemma"]

def test_copy2():
    w1= Word(dict4())
    w2 = w1.copy()
    #testing the normal usage of copy
    w1 = w2.copy()
    assert w2._props["lemma"] == w1. _props["lemma"]
    assert len(w2._props) == len(w1._props)
    
def test_copy3():
    w1= Word(dict4())
    w2 = w1.copy()
    w1 = w2.copy()
    #test if the word 2 is modified and not the word 1
    w1.del_prop("lemma") #memory test
    assert ("lemma" in w2._props) == True

def test_lemma_or_surface():
    w1 = Word({"lemma":"donkey"})
    w2 = Word({"surface":"kong"})
    w3 = Word({})
    
    #testing the normal use of it
    assert w1.lemma_or_surface() == "donkey"
    assert w2.lemma_or_surface() == "kong"
    assert w3.lemma_or_surface() == ''

def test___eq__():
    
    #testing normal use
    w1 = Word(dict1())
    w2 = w1.copy()
    assert w1 == w2

    #testing normal use
    w1 = Word(dict4())
    assert w1 != w2
    
    #testing normal use
    w2 = w1.copy()
    w2._props.pop("lemma")
    w1 = Word({"surface":"Charles","pos":"noun","syn":"nsubj:1"})
    assert w1 == w2


def test___hash__():
    w1 = Word(dict2())
    w1Hash = hash(w1)
    w2 = Word(dict2())
    w2.del_prop("lemma")
    w2.set_prop("lemma","lmao")
    w2Hash = hash(w2)
    w3 = Word(dict1())
    w3Hash = hash(w3)
    
    #testing normal use
    assert w1Hash == w2Hash
    assert w3Hash != w2Hash
    
    w3.set_prop("syn","qqc:123")
    assert w1Hash == hash(w3)
    assert w1Hash != hash(Word({}))

def test_syn_encode():
    
    #testing normal use
    w = Word({})
    testEncode = w.syn_encode({("obj",3)})
    assert testEncode == "obj:4"
    testEncode2 = w.syn_encode([("nmod",5),("xcomp",12),("nsubj",-10)])
    assert len(testEncode2)==24
    assert ("nmod:6" in testEncode2) == True
    assert ("xcomp:13" in testEncode2) == True
    assert ("nsubj:-9" in testEncode2) == True

def test_syn_iter():
    
    #testing normal use
    w = Word(dict5())
    synIter = w.syn_iter(None)
    liste = []
    
    for element in synIter:
        pair= element[0]+":"+str(element[1]+1)
        assert pair in w.syn
        liste.append(pair)
    
    assert len(liste) - 1 == w.syn.count(';')
    assert len(liste) == len(w.syn.split(';'))

"""
How to check on stdrr

    w2 = Word({"surface":"tests","lemma":"test","pos":"noun","syn":"obj:3;nsubj12;det:12;root:0"})
    synIter = w2.syn_iter(util.CONTEXTLESS_CTXINFO)
    liste2 = []
    for element in synIter:
        pair= element[0]+":"+str(element[1]+1)
        assert pair in w2.syn
        liste2.append(pair)

    assert len(liste2) == 3 
    assert util.CONTEXTLESS_CTXINFO._prefix_str != ""
    
    util.CONTEXTLESS_CTXINFO = SimpleContextInfo("")

    w2 = Word({"surface":"tests","lemma":"test","pos":"noun","syn":"obj:3;nsubj12"})
    synIter2 = w2.syn_iter(util.CONTEXTLESS_CTXINFO)
    assert util.CONTEXTLESS_CTXINFO._prefix_str != ""
"""    
