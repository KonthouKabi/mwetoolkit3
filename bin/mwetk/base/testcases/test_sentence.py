#! /usr/bin/env python3
# -*- coding:UTF-8 -*-

from ..ngram import Ngram
from ..sentence import Sentence
from ..sentence import SentenceFactory
from ..word import Word
from ..mweoccur import MWEOccurrence
from ..candidate import Candidate

def Wordlist1():
    return [Word({"surface":"J'","lemma":"je","pos":"pronoun"}), Word({"surface":"ai","lemma":"avoir","pos":"verb"}), Word({"surface":"pris","lemma":"prendre","pos":"verb"}), Word({"surface":"une","lemma":"une","pos":"article"}), Word({"surface":"douche","lemma":"douche","pos":"noun"})]

def Wordlist2():
    return [Word({"surface":"He","lemma":"he","pos":"pronoun"}) , Word({"surface":"has","lemma":"have","pos":"verb"}), Word({"surface":"to","lemma":"to","pos":"adverb"}), Word({"surface":"switch","lemma":"switch","pos":"verb"}), Word({"surface":"the","lemma":"the","pos":"article"}), Word({"surface":"light","lemma":"light","pos":"noun"}),Word({"surface":"on","lemma":"on","pos":"adverb"})]

def Wordlist3():
    return [Word({"surface":"He","lemma":"he","pos":"pronoun"}) , Word({"surface":"blew","lemma":"blow","pos":"verb"}), Word({"surface":"up","lemma":"up","pos":"adverb"}), Word({"surface":"the","lemma":"the","pos":"article"}), Word({"surface":"plane","lemma":"plane","pos":"noun"})]

def Wordlist4():
    return [Word({"surface":"He","lemma":"he","pos":"pronoun"}) , Word({"surface":"likes","lemma":"like","pos":"verb"}), Word({"surface":"to","lemma":"to","pos":"preposition"}), Word({"surface":"play","lemma":"play","pos":"verb"}), Word({"surface":"super","lemma":"super","pos":"noun"}), Word({"surface":"smash","lemma":"smash","pos":"noun"}), Word({"surface":"bros","lemma":"bros","pos":"noun"}), Word({"surface":"ultimate","lemma":"ultimate","pos":"adjective"}), Word({"surface":"with","lemma":"with","pos":"preposition"}), Word({"surface":"his","lemma":"his","pos":"pronoun"}), Word({"surface":"favorite","lemma":"favorite","pos":"adj"}), Word({"surface":"character","lemma":"character","pos":"noun"}), Word({"surface":"named","lemma":"name","pos":"verb"}), Word({"surface":"Lucina","lemma":"Lucina","pos":"noun"})]

def Wordlist5():
    return [Word({"surface":"One","lemma":"one","pos":"noun"}) , Word({"surface":"two","lemma":"two","pos":"noun"}), Word({"surface":"three","lemma":"three","pos":"noun"}), Word({"surface":"four","lemma":"four","pos":"noun"}), Word({"surface":"five","lemma":"five","pos":"noun"}), Word({"surface":"six","lemma":"six","pos":"noun"}), Word({"surface":"seven","lemma":"seven","pos":"noun"}), Word({"surface":"eight","lemma":"eight","pos":"noun"}), Word({"surface":"nine","lemma":"nine","pos":"noun"})]


def test_make():
    sf1 = SentenceFactory()
    s1 = sf1.make(Wordlist2())
    assert s1.id_number == 1
    assert s1.word_list[1]._props["lemma"] == "have"
    assert len(s1.word_list) == 7

    s2 = sf1.make()
    assert s2.id_number == 2
    assert len(s2.word_list) == 0

def test__init__():
    s1 = Sentence(Wordlist1(), 10)
    assert s1.id_number == 10
    assert len(s1.word_list) == 5




def test_xwe_indexes_gappy():
    #The case where the MWE is separated (i.e: switch [SMTH] on)
    s2 = Sentence(Wordlist2(), 5)
    s2.mweoccurs.append(MWEOccurrence(Sentence(Wordlist2(), 5), Candidate(3, word_list=[Word({"lemma":"switch"}),  Word({"lemma":"on"})]), [3,6]))
    assert [[0],[1],[2],[3,6],[4],[5]] == s2.xwe_indexes()

def test_xwe_indexes_continuous():
    #The case where the MWE is not seperated (i.e: He blew up the plane)
    #                                                 ---- --
    s3 = Sentence(Wordlist3(),9)
    s3.mweoccurs.append(MWEOccurrence(Sentence(Wordlist3(), 9), Candidate(1, word_list=[Word({"lemma":"blow"}), Word({"lemma":"up"})]), [1,2]))
    assert [[0],[1,2],[3],[4]] == s3.xwe_indexes()

def test_xwe_indexes_without_mwes():
    #The case where there's no MWE in the sentence
    s4 = Sentence(Wordlist4(),21)
    assert [[0],[1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11],[12],[13]]==s4.xwe_indexes()

def test_xwe_indexes_gappy_and_continuous():
    #MIXED UP CASE
    s5 = Sentence(Wordlist5(),9)
    s5.mweoccurs.append(MWEOccurrence(Sentence(Wordlist5(),9), Candidate(1, word_list=[Word({"lemma":"six"}), Word({"lemma":"two"})]), [1,5]))
    s5.mweoccurs.append(MWEOccurrence(Sentence(Wordlist5(),9), Candidate(1, word_list=[Word({"lemma":"eight"}), Word({"lemma":"three"}), Word({"lemma":"five"})]), [2,4,7]))
    s5.mweoccurs.append(MWEOccurrence(Sentence(Wordlist5(),9), Candidate(1, word_list=[Word({"lemma":"four"}), Word({"lemma":"nine"})]), [3,8]))

    """
    Sentence:         1 2 3 4 5 6 7 8 9

    expressions:      * + ^ + *   + ^
    """            

    assert [[0],[1,5],[2,4,7],[3,8],[6]] == s5.xwe_indexes()
    


def test_xwes_gappy():
    #The case where the MWE is separated (i.e: switch [SMTH] on)
    s2 = Sentence(Wordlist2(), 5)
    s2.mweoccurs.append(MWEOccurrence(Sentence(Wordlist2(), 5), Candidate(3, word_list=[Word({"lemma":"switch"}),  Word({"lemma":"on"})]), [3,6]))
    list1 = []
    for lists in s2.xwes():
        list2=[]
        for word in lists:
            list2.append(word._props["surface"])
        list1.append(list2)
    assert list1 == [["He"],["has"],["to"],["switch","on"],["the"],["light"]]

def test_xwes_continuous():
    #The case where the MWE is not seperated (i.e: He blew up the plane)
    #                                                 ---- --
    s3 = Sentence(Wordlist3(),9)
    s3.mweoccurs.append(MWEOccurrence(Sentence(Wordlist3(), 9), Candidate(1, word_list=[Word({"lemma":"blow"}), Word({"lemma":"up"})]), [1,2]))
    list1 = []
    for lists in s3.xwes():
        list2=[]
        for word in lists:
            list2.append(word._props["surface"])
        list1.append(list2)
    assert list1 == [["He"],["blew","up"],["the"],["plane"]]

def test_xwes_without_mwes():
    #The case where there's no MWE in the sentence
    s4 = Sentence(Wordlist4(),21)
    list1 = []
    for lists in s4.xwes():
        list2=[]
        for word in lists:
            list2.append(word._props["surface"])
        list1.append(list2)
    assert list1 == [["He"],["likes"],["to"],["play"],["super"],["smash"],["bros"],["ultimate"],["with"],["his"],["favorite"],["character"],["named"],["Lucina"]]

def test_xwes_gappy_and_continuous():
    #MIXED UP CASE
    s5 = Sentence(Wordlist5(),9)
    s5.mweoccurs.append(MWEOccurrence(Sentence(Wordlist5(),9), Candidate(1, word_list=[Word({"lemma":"six"}), Word({"lemma":"two"})]), [1,5]))
    s5.mweoccurs.append(MWEOccurrence(Sentence(Wordlist5(),9), Candidate(1, word_list=[Word({"lemma":"eight"}), Word({"lemma":"three"}), Word({"lemma":"five"})]), [2,4,7]))
    s5.mweoccurs.append(MWEOccurrence(Sentence(Wordlist5(),9), Candidate(1, word_list=[Word({"lemma":"four"}), Word({"lemma":"nine"})]), [3,8]))
    list1 = []
    for lists in s5.xwes():
        list2=[]
        for word in lists:
            list2.append(word._props["surface"])
        list1.append(list2)

    """
    Sentence:         1 2 3 4 5 6 7 8 9

    expressions:      * + ^ + *   + ^
    """            
    assert list1 == [["One"],["two","six"],["three","five","eight"],["four","nine"],["seven"]]
