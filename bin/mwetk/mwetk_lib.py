from mwetk.counter import append_counters_to_candidates
from mwetk.index import build_index
from mwetk.candidates import get_patterns, extract_candidates_from_file
from mwetk.feat_association import append_metrics_to_candidates, ALL_MEASURES

import pandas as pd

def build_df_from_candidates(candidates, dataset_name, measures):
    """ 
    Builds pandas Dataframe from a list of Candidates w/ 
    already computed association measures
    """
    columns = ['ngram', 'pos', 'counter'] + measures
    infos = {col: [] for col in columns}
    for candidate in candidates:
        infos['ngram'].append(' '.join([x.lemma for x in candidate.word_list]))
        infos['pos'].append(' '.join([x.pos for x in candidate.word_list]))
        infos['counter'].append(
            candidate.freqs.get_feature(dataset_name, -1).value
        )

        for measure in measures:
            feature_name = f"{measure}_{dataset_name}"
            if measure == "ll" and len(candidate.word_list) != 2:
                infos[measure].append(None)
            else:
                infos[measure].append(
                    candidate.features.get_feature(feature_name, -1).value
                )

    return pd.DataFrame(infos)

################################################################################


def get_candidates_dataframe(patterns_file_path, corpus_file_path, dataset_name, measures=None):
    """
    Builds pandas Dataframe from a pattern file and a corpus file.

    @param patterns_file_path Path of a patterns file in a format supported by MWEToolkit

    @param corpus_file_path Path of a corpus file in a format supported by MWEToolkit

    @param dataset_name Dataset's name

    @param measures Measures to be computed for all candidates
    """

    if measures is None:
        measures = ALL_MEASURES
    else:
        for measure in measures:
            if not measure in ALL_MEASURES:
                raise Exception(f"Unknown measure {measure}. Supported measures are: {', '.join(ALL_MEASURES)}")

    patterns = get_patterns([patterns_file_path])

    cands = extract_candidates_from_file(dataset_name, corpus_file_path, patterns)

    attr_combination = "lemma+pos"
    index = build_index(
        dataset_name,
        [corpus_file_path],
        simples_attributes=["lemma", "pos"],
        composite_attributes=["lemma+pos"],
        ctxinfo=None
    )

    corpus_size_dict = {dataset_name: index.metadata["corpus_size"]}

    append_counters_to_candidates(index, attr_combination, cands, dataset_name)

    append_metrics_to_candidates(cands, corpus_size_dict, measures)

    df = build_df_from_candidates(cands, dataset_name, measures)

    return df
