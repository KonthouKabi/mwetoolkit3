from mwetk.filetype import indexlib

from mwetk.filetype.indexlib import Index, ATTRIBUTE_SEPARATOR


def build_entry_builder(composite_rule):
    """ Builds a builder function for a composite rule, which can be a combination
    of surface, lemma and pos
    """
    attributes = composite_rule.split('+')

    def build_entry(surface, lemma, pos):
        # Only adds to Entry if attribute was in composite rule
        surface = surface if "surface" in attributes else None
        lemma = lemma if "lemma" in attributes else None
        pos = pos if "pos" in attributes else None

        return ATTRIBUTE_SEPARATOR.join([
            token for token in [surface, lemma, pos] if token
        ])
    return build_entry

################################################################################


def build_index(corpus_name,
                corpus_file_list,
                simples_attributes=["surface", "lemma", "pos"],
                composite_attributes=["lemma+pos"],
                ctxinfo=None):
    """
    Builds index w/ attributes for a corpus.

    @param corpus_name Name of the corpus to be indexed

    @param corpus_file_list List of file paths w/ text files from corpus

    @param simples_attributes Simple attributes for which indices will be generated

    @param composite_attributes Composite attributes (compostion of simple attributes)
    or which indices will be generated
    """

    index = indexlib.Index(corpus_name, simples_attributes, ctxinfo)
    index.populate_index(corpus_file_list, None, ctxinfo)
    for attrs in composite_attributes:
        index.make_fused_array(attrs.split('+'), ctxinfo)
    return index
